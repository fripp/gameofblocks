#  A game of blocks


## Architectural overview
Following the MVP approach, a very simple MVVM architecture has been implemented.

The class *BlockGameViewController* and all the *UIView* instances used for the UI represent the *View* layer of the architecture.

The class *BlockGameViewModel* represents the *View model* layer.

The *ColumnDescriptor* type represents the *Model* layer.

## Implementation details

### A cylindrical game board
For the implementation of the "bridge rule" I decided to consider the game board as a cylindrical surface.

In particular, the column on the left of the leftmost column on the board is the rightmost. The column on the right of the rightmost column of the board is the leftmost.

That said, the "bridge rule" has been implemented according to specifications.

### Bitsets
Each column of the board is represented by an n-bit bitset (by default 5 bits).

The board is an array of bitset.

The most significant bit of each bitset represents the position close to the bottom of the board. 

The least significant bit, on the other hand, represents the topmost position.

The bitset is modeled using the *ColumnDescriptor* type, consisting of a ***length*** *UInt* property (representing the size of the word) and a ***bitset*** *UInt* property (representing the current content of the column).

All the operations needed to check the availability of a certain position are performed using bitwise operators available in Swift.