//
//  Array+Extensions.swift
//  BlockGame
//
//  Created by feanor on 18/04/21.
//

import Foundation

extension Array where Element == ColumnDescriptor {
	func column(before: UInt) throws -> ColumnDescriptor {
		guard !isEmpty else {
			throw ColumnDescriptorError.emptyArray
		}

		if before == 0 {
			return last!
		} else {
			return self[Int(before)-1]
		}
	}

	func column(after: UInt) throws -> ColumnDescriptor {
		guard !isEmpty else {
			throw ColumnDescriptorError.emptyArray
		}

		if after == count - 1 {
			return first!
		} else {
			return self[Int(after) + 1]
		}
	}

	subscript(index: UInt) -> ColumnDescriptor {
		get {
			self[Int(index)]
		}

		set(newValue) {
			self[Int(index)] = newValue
		}
	}
}
