//
//  UInt+Extensions.swift
//  BlockGame
//
//  Created by feanor on 18/04/21.
//

import Foundation

extension UInt {
	/// Returns `true`if the bit at the specified position is set (i.e. equal to 1)
	///
	/// - Parameter position: The position of the bit we want to test
	/// - Returns: `true`if the bit is set, `false` otherwise
	func isBitSetAtPosition(_ position: UInt) -> Bool {
		let mask: UInt = 1 << position
		let bitwiseAnd: UInt = self & mask
		return bitwiseAnd != 0
	}
}
