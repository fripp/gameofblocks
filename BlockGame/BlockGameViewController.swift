//
//  BlockGameViewController.swift
//  BlockGame
//
//  Created by feanor on 18/04/21.
//

import UIKit
import Combine

private let spacing: CGFloat = 6
private let leadingMargin: CGFloat = 50

final class BlockGameViewController: UIViewController {

    private var blocks: [UIView] = []

    private var setOfCancellable: Set<AnyCancellable> = Set()

    private let viewModel: BlockGameViewModeling = BlockGameViewModel(numberOfRows: 5, numberOfColumns: 5, maxMoves: 10)

    private lazy var overlayView: UIView = UIView(frame: .zero)

    private let animationEndedSubject: CurrentValueSubject<Bool, Never> = CurrentValueSubject(true)

    private var numberOfRows: Int {
        viewModel.numberOfRows
    }

    private var numberOfColumns: Int {
        viewModel.numberOfColumns
    }

    private lazy var squareWidth: CGFloat =
            (UIScreen.main.bounds.width - leadingMargin - leadingMargin - ((CGFloat(numberOfRows) - 1) * spacing))
                    / CGFloat(numberOfRows)

    @IBOutlet weak var boardStackView: UIStackView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    @IBOutlet weak var gameEndedLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!

    deinit {
        for cancellable in setOfCancellable {
            cancellable.cancel()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        title = "Block game"

        // The game board is build using a vertical stack view, containing various horizontal stack views
        // The horizontal stack views contain the squares the user will see on the board
        drawBoard()

        // The user doesn't interact directly with the stack view mentioned above.
        // It interacts with an overlay view covering entirely the game board
        // It is the view that "feels" these user touch
        drawOverlay()

        // Let's bind the view model and the view
        bindViewModel()

        resetButton.layer.cornerRadius = 20
    }

    // MARK: Private methods

    /// Resets the board and call reset on the view model
    @objc private func reset(sender: UIButton) {
        for view in blocks {
            view.removeFromSuperview()
        }

        blocks = []
        viewModel.reset()
    }

    /// A gesture recognizer handler invoked every time the user taps on the overlay view
    @objc private func tapOnOverlay(sender: UITapGestureRecognizer) throws {
        let point = sender.location(in: sender.view)

        let denominator = squareWidth + spacing

        let column = UInt(point.x / denominator)
        let row = UInt(point.y / denominator)

        let coordinates: BlockGamePosition = BlockGamePosition(row: row, column: column)
        let positionAvailable = try viewModel.isPositionAvailable(coordinates)

        if !positionAvailable {
            try dropSquare(row: row, column: column)
        } else {
            print("occupied \(coordinates)")
        }
    }

    /// Binds the view with the view model
    private func bindViewModel() {
        viewModel.score
                .map { String(describing: $0) }
                .receive(on: RunLoop.main)
                .sink(receiveCompletion: { _ in  }, receiveValue: { [weak self] text in
                    self?.scoreLabel.text = text
                })
                .store(in: &setOfCancellable)

        // We disable the user interaction when the game is completed
        // (i.e. the user have reached the maximum number of moves possible)
        // or when an animation is in progress
        Publishers
                .CombineLatest(viewModel.gameEnded, animationEndedSubject)
                .map({ !$0 && $1 })
                .receive(on: RunLoop.main)
                .sink(receiveCompletion: { _ in  }, receiveValue: { [weak self] isUserInteractionEnabled in
                    self?.overlayView.isUserInteractionEnabled = isUserInteractionEnabled
                })
                .store(in: &setOfCancellable)

        viewModel.remainingMoves
                .map({ String(describing: $0) })
                .receive(on: RunLoop.main)
                .sink(receiveCompletion: { _ in  }, receiveValue: { [weak self] text in
                    self?.roundLabel.text = text
                })
                .store(in: &setOfCancellable)

        viewModel.gameEnded
                .map({ !$0 })
                .receive(on: RunLoop.main)
                .sink(receiveCompletion: { _ in  }, receiveValue: { [weak self] isHidden in
                    self?.gameEndedLabel.isHidden = isHidden
                })
                .store(in: &setOfCancellable)

        resetButton.addTarget(self, action: #selector(reset), for: .touchUpInside)
    }

    /// Draws the board on the screen
    private func drawBoard() {
        for _ in 0..<numberOfRows {
            boardStackView.addArrangedSubview(buildRow(numberOfColumns: numberOfColumns, spacing: spacing))
        }
    }

    /// Draws the overlay view onto which the user will place blocks
    private func drawOverlay() {
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        overlayView.isUserInteractionEnabled = true
        overlayView.backgroundColor = .clear

        view.addSubview(overlayView)

        overlayView.leadingAnchor.constraint(equalTo: boardStackView.leadingAnchor).isActive = true
        overlayView.trailingAnchor.constraint(equalTo: boardStackView.trailingAnchor).isActive = true
        overlayView.bottomAnchor.constraint(equalTo: boardStackView.bottomAnchor).isActive = true
        overlayView.topAnchor.constraint(equalTo: boardStackView.topAnchor).isActive = true

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapOnOverlay))
        overlayView.addGestureRecognizer(tapGesture)
    }

    /// Drops a square on the overlay view, on the given position
    /// - Parameter row: the row in which the square will be
    /// - Parameter column: the column in which the square will be
    private func dropSquare(row: UInt, column: UInt) throws {
        let square = generateSquareView(width: squareWidth, color: .systemRed, useAutoLayout: false)

        square.frame = rectFor(row: row, column: column, squareWidth: squareWidth, spacingBetweenSquare: spacing)

        print(square.frame)

        let coordinates = BlockGamePosition(row: row, column: column)
        let destination = try viewModel.generateDestinationPosition(coordinates)
        overlayView.addSubview(square)
        blocks.append(square)

        if destination != coordinates {
            print("Destination \(destination)")

            let newFrame = rectFor(
                    row: destination.row,
                    column: destination.column,
                    squareWidth: squareWidth,
                    spacingBetweenSquare: spacing)

            animationEndedSubject.send(false)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in square.frame = newFrame }, completion: { _ in
                self.animationEndedSubject.send(true)
            })
        }

    }

    /// Calculates the frame for a certain square, give the column and the row
    /// - Parameter row: the row in which the square is
    /// - Parameter column: the column in which the square is
    /// - Parameter squareWidth: the width of the square
    /// - Parameter spacingBetweenSquare: the spacing between squares
    /// - Returns: A `CGRect` instance describing the frame of the square
    private func rectFor(row: UInt, column: UInt, squareWidth: CGFloat, spacingBetweenSquare: CGFloat) -> CGRect {
        let offsetX: CGFloat = spacingBetweenSquare * CGFloat(column)
        let offsetY: CGFloat = spacingBetweenSquare * CGFloat(row)
        let newFrameX: CGFloat = CGFloat(column) * squareWidth + offsetX
        let newFrameY: CGFloat = CGFloat(row) * squareWidth + offsetY
        return CGRect(x: newFrameX, y: newFrameY, width: squareWidth, height: squareWidth)
    }

    /// Builds a row for the game board
    /// - Parameter numberOfColumns: the number of columns in a row
    /// - Parameter spacing: the spacing between columns
    /// - Returns: An instance of UIStackView
    private func buildRow(numberOfColumns: Int, spacing: CGFloat) -> UIStackView {
        let squares = (0..<numberOfColumns).map({ _ -> UIView in
            self.generateSquareView(width: squareWidth, color: .systemBlue, useAutoLayout: true)
        })

        let stack = UIStackView(arrangedSubviews: squares)
        stack.axis = .horizontal
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = spacing
        stack.distribution = .fillEqually

        return stack
    }

    /// Generates a square view to be used for the game
    /// - Parameter width: the width of the square
    /// - Parameter color: the background color of the square
    /// - Parameter useAutoLayout: a boolean indicating whether create a view ready to be used in outolayout or no
    /// - Returns: An instance of UIView
    private func generateSquareView(width: CGFloat, color: UIColor, useAutoLayout: Bool) -> UIView {

        let square = UIView(frame: .zero)
        square.translatesAutoresizingMaskIntoConstraints = false
        square.backgroundColor = color
        if useAutoLayout {
            square.heightAnchor.constraint(equalToConstant: width).isActive = true
            square.widthAnchor.constraint(equalToConstant: width).isActive = true
        } else {
            square.frame = CGRect(x: 0, y: 0, width: width, height: width)
        }

        return square
    }
}
