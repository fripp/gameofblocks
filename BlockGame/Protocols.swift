//
//  Protocols.swift
//  BlockGame
//
//  Created by feanor on 18/04/21.
//

import Combine
import Foundation

/// A protocol describing a view model for a "Game of blocks" game
protocol BlockGameViewModeling {

    /// The number of rows available on the game board
    var numberOfRows: Int { get }

    /// The number of columns available on the game board
    var numberOfColumns: Int { get }

    /// The maximum number of moves allowed during a game
    var maxMoves: Int { get }

    /// A `Publisher` that publishes in real time the current score during the game
    var score: AnyPublisher<Int, Never> { get }

    /// A `Publisher` publishing in real time the number of remaining moves in the current game
    var remainingMoves: AnyPublisher<Int, Never> { get }

    /// A `Publisher` publishing a `true` once the game is completed
    var gameEnded: AnyPublisher<Bool, Never> { get }

    /// Resets the current game
    func reset()

    /// Returns `true` if the current position on the board is available, otherwise `false`
    ///
    /// - Parameter position: A `BlockGamePosition` instance, describing the position we want to check for availability
    /// - Returns: `true` if the position on the board is available, otherwise `false`
    /// - Throws: An exception in case some corner case is happening
    func isPositionAvailable(_ position: BlockGamePosition) throws -> Bool

    /// Returns the destination position of a block once the user has touched a square in a certain position
    ///
    /// - Parameter selectedPosition: A `BlockGamePosition` instance, describing the position selected by the user
    /// - Returns: An instance of `BlockGamePosition` representing the next position occupied by the block. The position is determined applying the rules of the game
    /// - Throws: An exception in case some corner case is happening
    func generateDestinationPosition(_ selectedPosition: BlockGamePosition) throws -> BlockGamePosition
}
