//
//  BlockGameViewModel.swift
//  BlockGame
//
//  Created by feanor on 18/04/21.
//

import Combine
import Foundation

/// A position on the board
struct BlockGamePosition {
	let row: UInt
	let column: UInt
}

/// A bitset describing a column on the game board
struct ColumnDescriptor {
	let length: UInt
	let bitset: UInt

	/// Generates an empty column
	static func empty(length: UInt) -> ColumnDescriptor {
		ColumnDescriptor(length: length, bitset: 0)
	}
}

enum ColumnDescriptorError: Error {
	case fullColumn
	case emptyArray
	case outOfBounds
}

extension BlockGamePosition: Equatable {}

private extension BlockGamePosition {
	/// Generates a new `BlockGamePosition` keeping the same column but updating the row
	func coordinate(row newRow: UInt) -> BlockGamePosition {
		BlockGamePosition(row: newRow, column: column)
	}

	/// Generates a new `BlockGamePosition` keeping the same row but updating the column
	func coordinate(column newColumn: UInt) -> BlockGamePosition {
		BlockGamePosition(row: row, column: newColumn)
	}
}

private extension ColumnDescriptor {
	/// Calculates the score of a column, according to the rules
	var score: Int {
		// If the bitset is empty, the score is zero
		if bitset == 0 {
			return 0
		} else {
			var totalScore = 0

			// The following code calculates, from the most significant to the least one, the number of bits set to '1'
			// and the number of bits set to '0'
			//
			var numberOf1 = 0
			var numberOf0 = 0
			for currentBit in (0..<length).reversed() {
				if bitset.isBitSetAtPosition(currentBit) {
					numberOf1 += 1
					totalScore += numberOf1 * 5

					if numberOf0 > 0 {
						totalScore += numberOf0 * 10
						numberOf0 = 0
					}
				} else {
					numberOf0 += 1
				}
			}

			return totalScore
		}
	}

	func columnDescriptor(occupiedRow: UInt) -> ColumnDescriptor {
		let newMask = bitset | (1 << occupiedRow)
		return ColumnDescriptor(length: length, bitset: newMask)
	}
}

private extension ColumnDescriptor {

	var isEmpty: Bool {
		bitset == 0
	}

	/// Returns `true`if the specified row is occupied, `false` elsewhere
	///
	/// - Parameter row: The row we want to check
	/// - Returns: `true`if the specified row is occupied, `false` elsewhere
	/// - Throws: A `ColumnDescriptorError` in case of problems
	func isRowOccupied(_ row: UInt) throws -> Bool {

        guard row < length else {
            throw ColumnDescriptorError.outOfBounds
        }

		return bitset.isBitSetAtPosition(row)
	}

	/// Returns the most significant row available on a certain column
	///
	/// - Returns: The index of the most significant bit still available
	/// - Throws: A `ColumnDescriptorError` in case of problems
	func mostSignificantAvailableRow() throws -> UInt {
		let start = length - 1

		var max: UInt?

		for index in (0...start) {

			if !bitset.isBitSetAtPosition(index) {
				if max == nil {
					max = index
				} else if let currentMax = max, index > currentMax {
					max = index
				}
			} else {
				break
			}
		}

		guard let currentMax = max else {
			throw ColumnDescriptorError.fullColumn
		}

		return currentMax

	}
}

final class BlockGameViewModel: BlockGameViewModeling {

	let numberOfRows: Int
	let numberOfColumns: Int
	let maxMoves: Int

	private let scoreSubject: CurrentValueSubject<Int, Never>
	private let blockCountSubject: CurrentValueSubject<Int, Never>

	private var columns: [ColumnDescriptor] {
		didSet {
			let score = calculateScore()
			scoreSubject.send(score)
		}
	}

	var score: AnyPublisher<Int, Never> {
		scoreSubject.removeDuplicates().eraseToAnyPublisher()
	}

	var gameEnded: AnyPublisher<Bool, Never> {
		blockCountSubject
				.map({ $0 == self.maxMoves })
				.removeDuplicates()
				.eraseToAnyPublisher()
	}

	var remainingMoves: AnyPublisher<Int, Never> {
		blockCountSubject
				.map({ self.maxMoves - $0 })
				.eraseToAnyPublisher()
	}

	init(numberOfRows rows: Int, numberOfColumns columns: Int, maxMoves max: Int) {
		let maxMovesPossible = rows * columns

		numberOfRows = rows
		numberOfColumns = columns

		maxMoves = max > maxMovesPossible ? maxMovesPossible : max
		self.columns = [ColumnDescriptor](repeating: .empty(length: UInt(numberOfColumns)), count: numberOfColumns)

		scoreSubject = CurrentValueSubject(0)
		blockCountSubject = CurrentValueSubject(0)
	}

	func isPositionAvailable(_ position: BlockGamePosition) throws -> Bool {
		let column = columns[position.column]
		return try column.isRowOccupied(position.row)
	}

	func generateDestinationPosition(_ selectedPosition: BlockGamePosition) throws -> BlockGamePosition {

		let currentColumn = columns[selectedPosition.column]
		let columnBefore = try columns.column(before: selectedPosition.column)
		let columnAfter = try columns.column(after: selectedPosition.column)

		let currentColumnMinimumFreeIndex = try currentColumn.mostSignificantAvailableRow()
        
		let occupiedRow: UInt

		// In this check is true, we are in the "under the bridge" scenario (i.e. the user taps on a free spot under a bridge)
		// In this scenario the selected spot it's the destination spot
        if try (columnBefore.isRowOccupied(selectedPosition.row) && columnAfter.isRowOccupied(selectedPosition.row)) {
            occupiedRow = selectedPosition.row

            columns[selectedPosition.column] = currentColumn.columnDescriptor(occupiedRow: occupiedRow)

            blockCountSubject.value += 1

            return selectedPosition.coordinate(row: occupiedRow)
        } else {

	        // Let the square "fall" and in case there's a bridge, let's stop and use the current position as the destination
            for index in selectedPosition.row ..< currentColumnMinimumFreeIndex {
                if try (columnBefore.isRowOccupied(index) && columnAfter.isRowOccupied(index)) {
                    occupiedRow = index
                    columns[selectedPosition.column] = currentColumn.columnDescriptor(occupiedRow: occupiedRow)

                    blockCountSubject.value += 1

                    return selectedPosition.coordinate(row: occupiedRow)
                }
            }
            occupiedRow = currentColumnMinimumFreeIndex
            columns[selectedPosition.column] = currentColumn.columnDescriptor(occupiedRow: occupiedRow)

            blockCountSubject.value += 1

            return selectedPosition.coordinate(row: occupiedRow)
        }
	}

	func reset() {
		blockCountSubject.value = 0
		let repeatingValue = ColumnDescriptor(length: UInt(numberOfColumns), bitset: 0)
		columns = [ColumnDescriptor](repeating: repeatingValue, count: numberOfColumns)
	}

	// MARK: Private methods

	/// Returns the current score
	private func calculateScore() -> Int {
		columns.map(\.score).reduce(0) { $0 + $1 }
	}
}
