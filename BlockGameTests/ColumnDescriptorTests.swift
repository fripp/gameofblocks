//
//  BlockGamePositionTests.swift
//  BlockGameTests
//
//  Created by feanor on 18/04/21.
//

import XCTest
@testable import BlockGame

class ColumnDescriptorTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testEmptyColumnScoreZero() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let empty = ColumnDescriptor.empty(length: 5)
        
        XCTAssertEqual(empty.score, 0)
        
    }
    
    func testNotEmptyColumScore() throws {
        let column = ColumnDescriptor(length: 5, bitset: 16) // 10000
        
        XCTAssertEqual(column.score, 5)
    }
    
    func testNotEmptyColumScore2() throws {
        let column = ColumnDescriptor(length: 5, bitset: 24) // 11000
        
        XCTAssertEqual(column.score, 15)
    }
    
    func testFullColumnScore() throws {
        let column = ColumnDescriptor(length: 5, bitset: 31) // 11111
        
        XCTAssertEqual(column.score, 75)
    }
    
    func testSparseColumnScore() throws {
        let column = ColumnDescriptor(length: 5, bitset: 18) // 10010
        
        XCTAssertEqual(column.score, 35)
    }
    
    func testSparseColumnScore2() throws {
        let column = ColumnDescriptor(length: 5, bitset: 17) // 10001
        
        XCTAssertEqual(column.score, 45)
    }

    func testColumnDescriptorBuilder() throws {
        let startingColumn = ColumnDescriptor(length: 5, bitset: 0)
        
        let newColumn = startingColumn.columnDescriptor(occupiedRow: 1)
        
        XCTAssertEqual(newColumn.bitset, 2)
    }
    
    
    func testIsEmpty() throws {
        let column = ColumnDescriptor(length: 5, bitset: 0)
        
        XCTAssertTrue(column.isEmpty)
    }
    
    func testIsRowOccupiedOnEmptyColumn() throws {
        let column = ColumnDescriptor(length: 5, bitset: 0)
        
        let array: [Bool] = try (0..<column.length).map({ try column.isRowOccupied($0) })
        
        XCTAssertTrue(array.allSatisfy({ !$0 }))
        
    }
    
    func testIsRowOccupiedOnFullColumn() throws {
        let column = ColumnDescriptor(length: 5, bitset: 31)
        
        let array: [Bool] = try (0..<column.length).map({ try column.isRowOccupied($0) })
        
        XCTAssertTrue(array.allSatisfy({ $0 }))
        
    }
    
    func testIsRowOccupiedOnSingleBitColumn() throws {
        let column = ColumnDescriptor(length: 5, bitset: 16)
                
        XCTAssertTrue(try column.isRowOccupied(4))
    }
    
    func testIsRowOccupiedRaisesException() throws {
        let column = ColumnDescriptor(length: 5, bitset: 16)
                
        XCTAssertThrowsError(try column.isRowOccupied(5))
    }
    
    func testMostSignificantAvailableRowOnEmptyColumn() throws {
        let column = ColumnDescriptor(length: 5, bitset: 0)
        
        let row = try column.mostSignificantAvailableRow()
        
        XCTAssertEqual(row, 4)
    }
    
    func testMostSignificantAvailableRowOnColumnWithPattern() throws {
        let column = ColumnDescriptor(length: 5, bitset: 16) // 10000
        
        let row = try column.mostSignificantAvailableRow()
        
        XCTAssertEqual(row, 3)
    }
    
    func testMostSignificantAvailableRowOnColumnWithPattern2() throws {
        let column = ColumnDescriptor(length: 5, bitset: 20) // 10100
        
        let row = try column.mostSignificantAvailableRow()
        
        XCTAssertEqual(row, 1)
    }
    
    func testMostSignificantAvailableRowOnColumnWithPattern3() throws {
        let column = ColumnDescriptor(length: 5, bitset: 21) // 10101
                
        XCTAssertThrowsError(try column.mostSignificantAvailableRow())
    }
    
    func testMostSignificantAvailableRowOnFullColumn() throws {
        let column = ColumnDescriptor(length: 5, bitset: 31) // 10101
                
        XCTAssertThrowsError(try column.mostSignificantAvailableRow())
    }
}
